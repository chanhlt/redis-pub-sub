# Google Kubernetes Engine (GKE)

### Docker
1. Install Dokcer (Ubuntu 18.04)
```shell
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
docker --version
```
2. Build Docker image
    1. Create Dockerfile (This is located in root directory of **publisher** project)
    ```
    FROM openjdk:8-jdk-alpine
    VOLUME /tmp
    ARG JAR_FILE="target/publisher.jar"
    COPY ${JAR_FILE} app.jar
    COPY config.properties application.properties
    ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.config.location=file:/application.properties","-jar","/app.jar"]
    ```
    2. Build Docker image
    ```shell
    docker build -t publisher:1.0 .
    ```
    3. View Docker images
    ```shell
    docker images
    REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
    publisher                  1.0                 86bf5ce459cc        12 seconds ago      134MB
    ```

### Kubernetes
1. What is Kubernetes?
*Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications.*
[Kubernetes Home page](https://kubernetes.io/ "Kubenetes Home page")

2. Cluster, Node, Pod, Service
    1. *A **Node** is a virtual machine*
    ```shell
    kubectl get nodes
    NAME                                             STATUS    ROLES     AGE       VERSION
    gke-workflow-engine-default-pool-b1253944-bz1n   Ready     <none>    11d       v1.9.7-gke.3
    gke-workflow-engine-default-pool-b1253944-fzm9   Ready     <none>    11d       v1.9.7-gke.3
    gke-workflow-engine-default-pool-b1253944-xnr5   Ready     <none>    11d       v1.9.7-gke.3
    ```
    2. *A **Cluster** is made up of one or more Nodes*
    ```shell
    gcloud container clusters list
    NAME             LOCATION           MASTER_VERSION  MASTER_IP        MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
    workflow-engine  asia-northeast1-a  1.9.7-gke.3     104.198.126.154  n1-standard-1  1.9.7-gke.3   3          RUNNING
    ```
    3. *A **Pod** represents a running process on your cluster.*
    ```shell
    kubectl get pods
    NAME                              READY     STATUS    RESTARTS   AGE
    redis-76d55d8d56-mhw67            1/1       Running   0          5h
    workflow-engine-5f8f76d69-wcgjg   2/2       Running   0          1d
    ```
    4. *A **Service** is an abstraction which defines a logical set of Pods and a policy by which to access them - sometimes called a micro-service. *
    ```shell
    kubectl get services
    NAME              TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
    kubernetes        ClusterIP      10.19.240.1     <none>          443/TCP        11d
    redis             ClusterIP      10.19.248.150   <none>          6379/TCP       5h
    workflow-engine   LoadBalancer   10.19.242.134   35.190.226.28   80:30059/TCP   11d
    ```


### Google Kubernetes Engine
1. Create a cluster with 3 nodes
```shell
gcloud container clusters create workflow-engine --num-nodes=3
gcloud container clusters list
NAME             LOCATION           MASTER_VERSION  MASTER_IP        MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
workflow-engine  asia-northeast1-a  1.9.7-gke.3     104.198.126.154  n1-standard-1  1.9.7-gke.3   3          RUNNING
```
2. Create pods (deployments)
    1. Create Deployment file (**redis-deployment.yaml**)
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: redis
      labels:
        app: redis
    spec:
      selector:
        matchLabels:
          app: redis
      replicas: 1
      template:
        metadata:
          labels:
            app: redis
        spec:
          containers:
          - name: redis
            image: redis:4
            ports:
            - containerPort: 6379
    ```
    2. Run this file with **kubectl**
    ```shell
    kubectl apply -f redis-deployment.yaml
    ```
    ```shell
    kubectl get pods
    NAME                              READY     STATUS    RESTARTS   AGE
    redis-76d55d8d56-mhw67            1/1       Running   0          6h
    ```
3. Create services
    1. Create Service file (**redis-service.yaml**)
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: redis
      labels:
        app: redis
    spec:
      ports:
      - port: 6379
        targetPort: 6379
      selector:
        app: redis
    ```
    2. Run this file with **kubectl**
    ```shell
    kubectl apply -f redis-service.yaml
    ```
    ```shell
    kubectl get services
    NAME              TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
    kubernetes        ClusterIP      10.19.240.1     <none>          443/TCP        11d
    redis             ClusterIP      10.19.248.150   <none>          6379/TCP       5h
    ```


### Deploy Pub-Sub clients and connect to Redis service
1. Deploy Publisher
    1. Create docker image
    ```shell
    docker build -t asia.gcr.io/ohg-test-env/publisher:1.0 publisher
    ```
    2. Push image to Google private registry
    ```shell
    docker push asia.gcr.io/ohg-test-env/publisher:1.0
    gcloud container images list --repository=asia.gcr.io/ohg-test-env
    NAME
    asia.gcr.io/ohg-test-env/publisher
    ```
    3. Create Pod
        1. Create Deployment file (**publisher-deployment.yaml**)
        ```yaml
        apiVersion: extensions/v1beta1
        kind: Deployment
        metadata:
          name: publisher
          labels:
            app: publisher
        spec:
          replicas: 1
          selector:
            matchLabels:
              app: publisher
          template:
            metadata:
              labels:
                app: publisher
            spec:
              containers:
                - image: asia.gcr.io/ohg-test-env/publisher:1.0
                  name: publisher
                  env:
                    - name: MYSQL_DB_HOST
                      value: 127.0.0.1:3306
                    - name: MYSQL_DB_USER
                      valueFrom:
                        secretKeyRef:
                          name: cloudsql-db-credentials
                          key: username
                    - name: MYSQL_DB_PASSWORD
                      valueFrom:
                        secretKeyRef:
                          name: cloudsql-db-credentials
                          key: password
                  ports:
                    - containerPort: 8080
                      name: publisher
                - name: cloudsql-proxy
                  image: gcr.io/cloudsql-docker/gce-proxy:1.11
                  command: ["/cloud_sql_proxy",
                            "-instances=ohg-test-env:asia-northeast1:ohg-test-env-dev=tcp:3306",
                            "-credential_file=/secrets/cloudsql/credentials.json"]
                  volumeMounts:
                    - name: cloudsql-instance-credentials
                      mountPath: /secrets/cloudsql
                      readOnly: true
              volumes:
                - name: cloudsql-instance-credentials
                  secret:
                    secretName: cloudsql-instance-credentials
        ```
        2. Run this file with **kubectl**
        ```shell
        kubectl apply -f publisher-deployment.yaml
        ```
        3. Check result
        ```shell
        kubectl get pods
        NAME                              READY     STATUS    RESTARTS   AGE
        publisher-59cd9fb54f-5hkdz        2/2       Running   0          13h
        redis-76d55d8d56-mhw67            1/1       Running   0          21h
        ```
    4. Create Service
        1. Create Service file (**publisher-service.yaml**)
        ```yaml
        apiVersion: v1
        kind: Service
        metadata:
          labels:
            app: publisher
          name: publisher
        spec:
          type: LoadBalancer
          ports:
            - port: 80
              targetPort: 8080
              protocol: TCP
          selector:
            app: publisher
        ```
        2. Run this file with **kubectl**
        ```shell
        kubectl apply -f publisher-service.yaml
        ```
        3. Check result
        ```shell
        kubectl get services
        NAME              TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE
        kubernetes        ClusterIP      10.19.240.1     <none>           443/TCP        11d
        publisher         LoadBalancer   10.19.248.71    35.200.120.223   80:30915/TCP   14h
        redis             ClusterIP      10.19.248.150   <none>           6379/TCP       21h
        ```
2. Deploy Subscriber
    1. Create docker image
    ```shell
    docker build -t asia.gcr.io/ohg-test-env/subscriber:1.0 subscriber
    ```
    2. Push image to Google private registry
    ```shell
    docker push asia.gcr.io/ohg-test-env/subscriber:1.0
    gcloud container images list --repository=asia.gcr.io/ohg-test-env
    NAME
    asia.gcr.io/ohg-test-env/publisher
    asia.gcr.io/ohg-test-env/subscriber
    ```
    3. Create Pod
        1. Create Deployment file (**subscriber-deployment.yaml**)
        ```yaml
        apiVersion: extensions/v1beta1
        kind: Deployment
        metadata:
          name: subscriber
          labels:
            app: subscriber
        spec:
          replicas: 1
          selector:
            matchLabels:
              app: subscriber
          template:
            metadata:
              labels:
                app: subscriber
            spec:
              containers:
                - image: asia.gcr.io/ohg-test-env/subscriber:1.0
                  name: subscriber
                  ports:
                    - containerPort: 8082
                      name: subscriber
        ```
        2. Run this file with **kubectl**
        ```shell
        kubectl apply -f subscriber-deployment.yaml
        ```
        3. Check result
        ```shell
        kubectl get pods
        NAME                              READY     STATUS    RESTARTS   AGE
        publisher-59cd9fb54f-5hkdz        2/2       Running   0          13h
        redis-76d55d8d56-mhw67            1/1       Running   0          21h
        subscriber-dddd976b6-49rs2        1/1       Running   0          13h
        ```
    4. Create Service
        1. Create Service file (**subscriber-service.yaml**)
        ```yaml
        apiVersion: v1
        kind: Service
        metadata:
          labels:
            app: subscriber
          name: subscriber
        spec:
          type: LoadBalancer
          ports:
            - port: 80
              targetPort: 8082
              protocol: TCP
          selector:
            app: subscriber
        ```
        2. Run this file with **kubectl**
        ```shell
        kubectl apply -f subscriber-service.yaml
        ```
        3. Check result
        ```shell
        kubectl get services
        NAME              TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE
        kubernetes        ClusterIP      10.19.240.1     <none>           443/TCP        11d
        publisher         LoadBalancer   10.19.248.71    35.200.120.223   80:30915/TCP   14h
        redis             ClusterIP      10.19.248.150   <none>           6379/TCP       21h
        subscriber        LoadBalancer   10.19.240.24    35.189.135.115   80:30911/TCP   14h
        ```
3. Test result
    1. Publish message from Publisher
    ```shell
    curl -X POST http://35.200.120.223/add -d "name=new contract&type=1"
    {"id":10,"name":"new contract","type":1}
    ```
    2. Handle message in Subscriber
    ```shell
    kubectl logs -f subscriber-dddd976b6-49rs2
    Subscriber listener started.
    channel: test-channel, message: There is a new contract has been added!
    ```


### Source code
##### [Full source code](https://gitlab.com/chanhlt/redis-pub-sub "Full source code")

